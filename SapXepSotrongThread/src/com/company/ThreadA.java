package com.company;

import java.io.*;

public class ThreadA extends Thread {

    private Message message;

    public ThreadA(Message message) {
        this.message = message;
    }


    @Override
    public synchronized void run() {
        while (true) {
            String message = this.message.getMessage();
            if (message != null) {
                System.out.println(message);
                writeOutput(message);
            }
        }
    }

    private static void writeOutput(String message) {
        FileWriter fileWriter;
        BufferedWriter bufferedWriter = null;

        try {

            fileWriter = new FileWriter("output.txt", true);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(message);
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(bufferedWriter != null ) {
                    bufferedWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

