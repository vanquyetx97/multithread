package com.company;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Message {

    private BlockingQueue<String> blockingDeque = new ArrayBlockingQueue<>(1000);

    public String getMessage(){
        try {
            if (blockingDeque.isEmpty())
            return blockingDeque.take();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null ;
    }

    public void addMessage(String message) {
        try {
            blockingDeque.put(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
