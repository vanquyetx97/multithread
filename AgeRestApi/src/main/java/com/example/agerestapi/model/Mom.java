package com.example.agerestapi.model;

import com.example.agerestapi.service.CheckAgeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.concurrent.CountDownLatch;

public class Mom extends Thread {

    private final Logger logger = LoggerFactory.getLogger(CheckAgeService.class);

    CountVariable countVariable;
    private final String name;
    private final Integer age;
    private CountDownLatch countDownLatch;

    public Mom(CountDownLatch countDownLatch, CountVariable countVariable, String name, Integer age) {
        this.countDownLatch = countDownLatch;
        this.countVariable = countVariable;
        this.name = name;
        this.age = age;
    }

    @Override
    public void run() {
        boolean check = false;
        logger.info("mom start " + name);
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            if (checkAge(readFile())) {
                check = true;
                countVariable.setCount(countVariable.getCount() + 1);
                if (countVariable.getCount() >= 2) {
                    countDownLatch.countDown();
                }
                countDownLatch.countDown();
            }
            if (!check) {
                countDownLatch.countDown();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int readFile() throws IOException {
        int age = 0;
        FileInputStream fileInputStream;
        BufferedReader bufferedReader = null;
        try {
            fileInputStream = new FileInputStream("D:/New folder/AgeRestApi/src/main/resources/file/" + name + "mom.txt");
            bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                age = 2021 - (Integer.parseInt(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert bufferedReader != null;
        bufferedReader.close();
        return age;

    }

    private boolean checkAge(int age) {
        return this.age == age;
    }
}
