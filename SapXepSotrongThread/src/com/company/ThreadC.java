package com.company;

import java.io.*;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class ThreadC extends Thread {
    private List<Integer> list;

    private Message message;
    private CountDownLatch countDownLatch;

    public ThreadC(CountDownLatch countDownLatch,List<Integer> list, Message message) {
        this.countDownLatch = countDownLatch;
        this.list = list;
        this.message = message;
    }

    @Override
    public void run() {

        try {
            System.out.println("-------wait b-------");
            countDownLatch.await();
            System.out.println("-------start c -------");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            readFile();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private synchronized void readFile() throws IOException, InterruptedException {
        FileReader fileReader;
        BufferedReader bufferedReader = null;

        try {
            fileReader = new FileReader("search.txt");
            bufferedReader = new BufferedReader(fileReader);
            String line = bufferedReader.readLine();
            while (line != null) {
                if (search(Integer.parseInt(line)) != -1) {
                    String message = line + " --in position -- " + search(Integer.parseInt(line));
                    this.message.addMessage(message);
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert bufferedReader != null;

        bufferedReader.close();
        System.out.println("-------c end -------");
    }

    private int search(int n) {
        return list.indexOf(n);
    }
}
