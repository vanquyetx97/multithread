package com.example.agerestapi.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

public class UBND extends Thread {

    CountVariable countVariable;
    private final String name;
    private final Integer age;
    private CountDownLatch countDownLatch;

    public UBND(CountDownLatch countDownLatch,CountVariable countVariable, String name, Integer age) {
        this.countDownLatch = countDownLatch;
        this.countVariable = countVariable;
        this.name = name;
        this.age = age;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            try {

                checkAge(readFile());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            countVariable.setCount(countVariable.getCount() + 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        countVariable.setCountEndThread(countVariable.getCountEndThread() + 1);
    }

    private Date readFile() throws IOException, ParseException {
        Date birthday = new Date();
        FileInputStream fileInputStream;
        BufferedReader bufferedReader = null;

        try {
            fileInputStream = new FileInputStream("D:/New folder/AgeRestApi/src/main/resources/file/" + name + "ubnd.txt");
            bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                try {
                    birthday = new SimpleDateFormat("dd/MM/yyyy").parse(line);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        assert bufferedReader != null;

        bufferedReader.close();

        return birthday;
    }


    private int checkAge(Date birthday) {
        int age = 0;
        Calendar born = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        if (birthday != null) {
            now.setTime(new Date());
            born.setTime(birthday);
            age = now.get(Calendar.YEAR) - born.get(Calendar.YEAR);
            if (now.get(Calendar.DAY_OF_YEAR) < born.get(Calendar.DAY_OF_YEAR)) {
                age -= 1;
            }
        }
        return age;
    }
}