package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class ThreadB extends Thread {

    private List<Integer> list;

    private CountDownLatch countDownLatch;

    public ThreadB(List<Integer> list,CountDownLatch countDownLatch) {
        this.list = list;
        this.countDownLatch=countDownLatch;
    }

    @Override
    public void run() {
        try {
            readFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFile() throws IOException {
        FileReader fileReader;
        BufferedReader bufferedReader = null;

        try {
            fileReader = new FileReader("input.txt");
            bufferedReader = new BufferedReader(fileReader);
            String line ;
            while ((line = bufferedReader.readLine()) != null) {
                String[] parts = line.split(" ");
                for (int i = 0; i < parts.length; i++) {
                    try {
                        int a = Integer.parseInt(parts[i]);
                        list.add(a);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert bufferedReader != null;
        bufferedReader.close();

        Collections.sort(list);
        System.out.println("-------size b : " + list.size());
        countDownLatch.countDown();
        System.out.println("-------b end--------");
    }
}