package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class Main {
    public static List<Integer> list = new ArrayList<>();

    public static void main(String[] args) {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Message message = new Message();

        ThreadA a = new ThreadA(message);
        ThreadB b = new ThreadB(list,countDownLatch);
        ThreadC c = new ThreadC(countDownLatch,list,message);

        b.start();
        c.start();
        a.start();
    }
}
