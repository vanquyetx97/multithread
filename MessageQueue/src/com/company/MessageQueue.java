package com.company;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class MessageQueue {
    private static final int capacity = 10;
    private final LinkedList<Message> items = new LinkedList<>();
    private static int dequeueCount = 0;
    private static int enqueueCount = 0;
    private static final long startTime = System.nanoTime();


    public void send(Message message) throws InterruptedException {
        synchronized (this) {
            while (items.size() == capacity) {
                System.out.println("--Queue is full");
                break;
            }
            if (items.size() < capacity) {
                System.out.println("Producer in " + Thread.currentThread().getName() + " put in queue : ");
                items.addLast(message);
                enqueueCount++;
                System.out.println("The current size in queue: " + items.size());
                System.out.println("Inside queue: ");
                items.forEach(message1 -> System.out.println(message1.toString()));
            }
            if (TimeUnit.SECONDS.convert(System.nanoTime() - startTime, TimeUnit.NANOSECONDS) == 60) {
                System.out.println("Enqueue : " + enqueueCount + "/" + "minutes");
                System.out.println("Dequeue : " + dequeueCount + "/" + "minutes");
                System.exit(0);
            }
        }
    }


    public Message take() throws InterruptedException {
        synchronized (this) {
            while (items.size() == 0) {
                System.out.println("Queue empty");
                wait(3000);
            }
            Message message = items.removeLast();
            dequeueCount++;
            System.out.println("Consumer in " + Thread.currentThread().getName() + " get : " + message);
            System.out.println("Queue size just has : " + items.size());


            if (TimeUnit.SECONDS.convert(System.nanoTime() - startTime, TimeUnit.NANOSECONDS) == 60) {
                System.out.println("Enqueue: " + enqueueCount + "/" + "minutes");
                System.out.println("Dequeue: " + dequeueCount + "/" + "minutes");
                System.exit(0);
            }
            return message;

        }

    }
}
