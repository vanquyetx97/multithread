package com.company;

public class Main {

    public static void main(String[] args) {
        MessageQueue messageQueue = new MessageQueue();
        Producer producer = new Producer(messageQueue);
        Producer producer1 = new Producer(messageQueue);
        Producer producer2 = new Producer(messageQueue);
        Consumer consumer = new Consumer(messageQueue);
        Consumer consumer1 = new Consumer(messageQueue);
        Consumer consumer2 = new Consumer(messageQueue);
        new Thread(producer).start();
        new Thread(producer1).start();
        new Thread(producer2).start();
        new Thread(consumer).start();
        new Thread(consumer1).start();
        new Thread(consumer2).start();
    }
}
