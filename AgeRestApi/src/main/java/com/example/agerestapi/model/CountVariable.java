package com.example.agerestapi.model;

public class CountVariable {
    private int count = 0;
    private int CountEndThread = 0;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCountEndThread() {
        return CountEndThread;
    }

    public void setCountEndThread(int countEndThread) {
        CountEndThread = countEndThread;
    }
}
