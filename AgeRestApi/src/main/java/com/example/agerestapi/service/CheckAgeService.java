package com.example.agerestapi.service;

import com.example.agerestapi.model.CountVariable;
import com.example.agerestapi.model.Dad;
import com.example.agerestapi.model.Mom;
import com.example.agerestapi.model.UBND;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.util.concurrent.CountDownLatch;

@Service
public class CheckAgeService {

    private final Logger logger = LoggerFactory.getLogger(CheckAgeService.class);

    public void check(String name, Integer age) throws InterruptedException {
        CountVariable countVariable = new CountVariable();
        CountDownLatch countDownLatch = new CountDownLatch(3);

        Dad dad = new Dad(countDownLatch,countVariable, name, age);
        Mom mom = new Mom(countDownLatch,countVariable, name, age);
        UBND ubnd = new UBND(countDownLatch,countVariable, name, age);

        dad.start();
        mom.start();
        ubnd.start();

        try {
            System.out.println("Waiting for 3 thread have started ... ");
            countDownLatch.await();
            System.out.println("Starting main Thread ... ");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        boolean checkAge = countVariable.getCount() >= 2;
        countVariable.setCount(0);

        logger.info("check success");
        write(name, checkAge);
    }


    public static void write(String name, boolean check) {
        try {
            FileWriter fw = new FileWriter(name + "result.txt");
            fw.write(String.valueOf(check));
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("success !!");
    }
}
