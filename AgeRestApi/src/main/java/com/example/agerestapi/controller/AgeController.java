package com.example.agerestapi.controller;

import com.example.agerestapi.service.CheckAgeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/age")
public class AgeController {

    private final Logger logger = LoggerFactory.getLogger(AgeController.class);

    @Autowired
    private CheckAgeService service;

    @GetMapping("/check")
    public void getAge(@RequestParam String name, @RequestParam Integer age) throws InterruptedException {
        logger.info("get logger");
        service.check(name, age);
    }
}
