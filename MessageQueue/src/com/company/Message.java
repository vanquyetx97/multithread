package com.company;

public class Message {
    private static int idTemp = 1;
    private int id;
    private String message;

    public Message(String message) {
        this.id = idTemp++;
        this.message = message;
    }

    @Override
    public String toString() {
        return "Message { " + "id = " + id + ", message = '" + message + '\'' + '}';
    }

}
